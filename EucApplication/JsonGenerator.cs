﻿using EucApplication.Models;
using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using System.Text.Json;

namespace EucApplication
{
    public class JsonGenerator
    {
        public string? Json { get; set; }

        public JsonGenerator([Bind("Id,FirstName,LastName,BirthCertificateNumber,DateOfBirth,Gender,Email,Citizenship,GdprAccepted")] User user) {

            var options3 = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
                WriteIndented = true
            };
            Json = JsonSerializer.Serialize(user, options3);
        }
    }
}
