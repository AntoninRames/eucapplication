﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Text.Json.Serialization;

namespace EucApplication.Models
{
    public class DropdownListsItems
    {

        [JsonIgnore]
        [NotMapped]
        public List<SelectListItem>? Genders { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "Muž", Text = "Muž" },
            new SelectListItem { Value = "Žena", Text = "Žena" },
            new SelectListItem { Value = "Nedefinováno", Text = "Nedefinováno" }

        };
        [JsonIgnore]
        [NotMapped]
        public List<SelectListItem>? Citizenships{ get; set; } 


        public List<SelectListItem> GetCountriesByIso3166()
        {
            List<SelectListItem>? Citizenships = new List<SelectListItem>();
            foreach (CultureInfo culture in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo country = new RegionInfo(culture.LCID);
               
                    
                Citizenships.Add(new SelectListItem { Value = country.ToString(), Text = country.ToString() });
                
                    
            }
            return Citizenships.ToList();
        }


    }
}
