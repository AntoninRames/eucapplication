﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EucApplication.Models
{
    public class User : DropdownListsItems
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(250)]
        [DisplayName("Příjmení")]
        public string LastName { get; set; }

        [StringLength(10, MinimumLength = 9, ErrorMessage = "Špatná délka rodného čísla")]
        [DisplayName("Rodné číslo")]
        public string? BirthCertificateNumber { get; set; }
        
        [Required]
        [DisplayName("Datum narození")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
       
        [Required]
        [MaxLength(50)]
        [DisplayName("Pohlaví")]
        public string Gender { get; set; }
        
        [Required]
        [EmailAddress]
        [MaxLength(250)]
        [DisplayName("Email")]
        public string Email { get; set; }
        
        [Required]
        [MaxLength(50)]
        [DisplayName("Státní příslušnost")]
        public string Citizenship { get; set; }
        
        [Required]
        [DisplayName("Přijmout GDPR")]
        public bool GdprAccepted { get; set; }




    }
}
