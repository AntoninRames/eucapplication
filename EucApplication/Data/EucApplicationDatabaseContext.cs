﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EucApplication.Models;

namespace EucApplication.Data
{
    public class EucApplicationDatabaseContext : DbContext
    {
        public EucApplicationDatabaseContext (DbContextOptions<EucApplicationDatabaseContext> options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }
    }
}
