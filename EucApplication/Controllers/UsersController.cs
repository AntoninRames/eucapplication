﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EucApplication.Data;
using EucApplication.Models;
using System.Text.Json;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http.Extensions;

namespace EucApplication.Controllers
{
    public class UsersController : Controller
    {
        private readonly EucApplicationDatabaseContext _context;
        private JsonGenerator jsonGenerator;
        public UsersController(EucApplicationDatabaseContext context)
        {
            _context = context;
        }


      
        // GET: Users/Create
        public IActionResult Create()
        {
            var model = new User();
            model.Citizenships = model.GetCountriesByIso3166();
            return View(model);
        }

        // POST: Users/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,BirthCertificateNumber,DateOfBirth,Gender,Email,Citizenship,GdprAccepted")] User user)
        {
            
            if (ModelState.IsValid)
            {
                jsonGenerator = new JsonGenerator(user);
                _context.Add(user);
                await _context.SaveChangesAsync();
                return GetJson(jsonGenerator);
            }         
            return View(user);
        }
      
        public IActionResult GetJson(JsonGenerator? generatedJson)
        {
            
            return View("~/Views/Json/GeneratedJson.cshtml", generatedJson);
        }

     
        public IActionResult ChangeCulture(string culture)
        {
            
            if (culture != null)
            {

                Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName, CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)), new CookieOptions { Expires = DateTime.UtcNow.AddDays(7) });
                return RedirectToAction("Create");
            }
           
           
           return RedirectToAction("Create");
        }
        public IActionResult OpenPdf() {



            string path = Path.Combine(Environment.CurrentDirectory, @"Files\PdfFileGdpr.pdf");
            byte[] FileBytes = System.IO.File.ReadAllBytes(path);

            return File(FileBytes, "application/pdf");
        }


    }
}
